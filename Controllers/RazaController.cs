using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using EjemploAdministracionPerrosApi.Model;
using Microsoft.AspNetCore.Authorization;

namespace EjemploAdministracionPerrosApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class RazaController : Controller
    {
        private readonly EjemploAdministracionPerrosContext context;
        
        public RazaController(EjemploAdministracionPerrosContext context)
        {
            this.context = context;
            
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<Raza> Get()
        {
            
            return context.Raza.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Raza Get(int id)
        {
            return context.Raza.Find(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]Raza value)
        {
            context.Raza.Add(value);
            context.SaveChanges();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Raza value)
        {
            var old  = context.Raza.Find(id);
            old.RazNombre = value.RazNombre;
            old.RazActivo = value.RazActivo;
            context.SaveChanges();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var old  = context.Raza.Find(id);
            context.Raza.Remove(old);
            context.SaveChanges();
        }

    }
}