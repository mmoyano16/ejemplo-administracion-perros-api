﻿using System;
using System.Collections.Generic;

namespace EjemploAdministracionPerrosApi.Model
{
    public partial class Raza
    {
        public Raza()
        {
            Perro = new HashSet<Perro>();
        }

        public int RazId { get; set; }
        public string RazNombre { get; set; }
        public bool RazActivo { get; set; }

        public virtual ICollection<Perro> Perro { get; set; }
    }
}
